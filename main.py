#!-*- coding:utf-8 -*-
import tornado
from tornado.ioloop import IOLoop
from tornado import gen, web, httpclient
from bs4 import BeautifulSoup


class IndexHandler(web.RequestHandler):
    @gen.coroutine
    def get(self):
	url = 'http://music.163.com/discover/toplist'
	id = self.get_argument('id', None)
	if id:
	    url = url + '?id=' + str(id)
	response = yield httpclient.AsyncHTTPClient().fetch(url)
	body = response.body if isinstance(response.body, str) else response.body.decode()
	soup = BeautifulSoup(body, 'lxml')
	#data = soup.find(id='toplist') #type list + song list
	data = soup.find(attrs={"class": "g-wrap12"}) # song list
	self.write(str(data))
	self.finish()


if __name__ == "__main__":
    app = tornado.web.Application(handlers=[(r"/", IndexHandler), (r"/discover/toplist", IndexHandler)])
    app.listen(8000)
    tornado.ioloop.IOLoop.current().start()
